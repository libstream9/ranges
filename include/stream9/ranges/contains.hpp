#ifndef STREAM9_RANGES_CONTAINS_HPP
#define STREAM9_RANGES_CONTAINS_HPP

#include "namespace.hpp"

#include <algorithm>
#include <concepts>
#include <functional>
#include <ranges>

namespace stream9::ranges {

namespace _contains {

    template<class R, class T, class Proj = std::identity>
    concept has_range_find =
        rng::input_range<R> &&
        requires (R&& r, T const& v, Proj p) {
            { rng::find(r, v, p) }
                -> std::convertible_to<rng::iterator_t<R const&>>;
        };

    struct api
    {
        template<rng::input_range R, class T, class Proj = std::identity>
            requires has_range_find<R, T, Proj>
        bool
        operator()(R&& r, T const& value, Proj proj = {}) const
        {
            return rng::find(r, value, proj) != rng::end(r);
        }

        template<rng::input_range R, class T, class Proj = std::identity>
            requires (!has_range_find<R, T, Proj>)
        bool
        operator()(R&& r, T const& value, Proj proj = {}) const
        {
            for (auto const& v: r) {
                if (proj(v) == value) return true;
            }

            return false;
        }
    };

} // namespace _contains

inline constexpr _contains::api contains;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_CONTAINS_HPP
