#ifndef STREAM9_RANGES_VIEWS_ZIP_HPP
#define STREAM9_RANGES_VIEWS_ZIP_HPP

#include "../namespace.hpp"

#include <ranges>

#include <stream9/iterators.hpp>

namespace stream9::ranges::views {

namespace _zip {

    struct api
    {
        template<rng::input_range... Rs>
        auto
        operator()(Rs&&... r) const
        {
            return rng::subrange(
                iter::zip_iterator(rng::begin(r)...),
                iter::zip_iterator(rng::end(r)...)
            );
        }
    };

} // namespace _zip

inline constexpr _zip::api zip;

} // namespace stream9::ranges::views

#endif // STREAM9_RANGES_VIEWS_ZIP_HPP
