#ifndef STREAM9_RANGES_RANGES_HPP
#define STREAM9_RANGES_RANGES_HPP

#include "namespace.hpp"

#include <ranges>
#include <stdexcept>

namespace stream9::ranges {

using namespace std::ranges; // import std::ranges

namespace _front {

    struct api {
        template<typename T>
            requires requires (T&& r) {
                *rng::begin(r);
            }
        decltype(auto)
        operator()(T&& r) const
        {
            return *rng::begin(r);
        }
    };

} // namespace _front

inline constexpr _front::api front;

namespace _back {

    struct api {
        template<typename T>
            requires requires (T&& r) {
                *rng::prev(rng::end(r));
            }
        decltype(auto)
        operator()(T&& r) const
        {
            return *rng::prev(rng::end(r));
        }
    };

} // namespace _back

inline constexpr _back::api back;

namespace _at {

    struct api {
        template<rng::forward_range T>
            requires requires (T&& r, rng::range_difference_t<T> const n) {
                *rng::next(rng::begin(r), n);
                rng::ssize(r);
            }
        decltype(auto)
        operator()(T&& r, rng::range_difference_t<T> const n) const
        {
            if (0 <= n && n < rng::ssize(r)) {
                return *rng::next(rng::begin(r), n);
            }
            else {
                throw std::out_of_range("stream9::ranges::at");
            }
        }
    };

} // namespace _at

inline constexpr _at::api at;

namespace _at_nocheck {

    struct api {
        template<rng::forward_range T>
            requires requires (T&& r, rng::range_difference_t<T> const n) {
                *rng::next(rng::begin(r), n);
            }
        decltype(auto)
        operator()(T&& r, rng::range_difference_t<T> const n) const
        {
            return *rng::next(rng::begin(r), n);
        }
    };

} // namespace _at_nocheck

inline constexpr _at_nocheck::api at_nocheck;

namespace _index_at {

    struct api
    {
        template<rng::random_access_range T>
        rng::range_difference_t<T>
        operator()(T&& r, rng::iterator_t<T> const it) const
        {
            return it - rng::begin(r);
        }
    };

} // namespace _index_at

inline constexpr _index_at::api index_at;

namespace _last_index_of {

    struct api
    {
        template<rng::random_access_range T>
        rng::range_difference_t<T>
        operator()(T&& r) const
        {
            return rng::end(r) - rng::begin(r) - 1;
        }
    };

} // namespace _last_index_of

inline constexpr _last_index_of::api last_index_of;

namespace _iterator_at {

    struct api
    {
        template<rng::random_access_range T, std::integral I>
        rng::iterator_t<T>
        operator()(T&& r, I const n) const
        {
            return rng::begin(r) + static_cast<rng::range_difference_t<T>>(n);
        }
    };

} // namespace _iterator_at

inline constexpr _iterator_at::api iterator_at;

namespace _unpack {

    struct api
    {
        template<rng::borrowed_range R>
        auto
        operator()(R&& r) const
        {
            return std::make_pair(
                rng::begin(std::forward<R>(r)),
                rng::end(std::forward<R>(r))
            );
        }
    };

} // namespace _unpack

inline constexpr _unpack::api unpack;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_RANGES_HPP
