#ifndef STREAM9_RANGES_NAMESPACE_HPP
#define STREAM9_RANGES_NAMESPACE_HPP

namespace std::ranges {}
namespace std::ranges::views {}
namespace stream9::iterators {}

namespace stream9::ranges {

namespace rng { using namespace ranges; }
namespace rng { using namespace std::ranges; }
namespace rng::views { using namespace std::ranges::views; }

namespace iter { using namespace stream9::iterators; }

} // namespace stream9::ranges

#endif // STREAM9_RANGES_NAMESPACE_HPP
