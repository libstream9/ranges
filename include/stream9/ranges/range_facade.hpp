#ifndef STREAM9_RANGES_RANGE_FACADE_HPP
#define STREAM9_RANGES_RANGE_FACADE_HPP

#include "ranges.hpp"
#include "namespace.hpp"

#include <ranges>

namespace stream9::ranges {

struct range_base {};

template<typename Derived>
class range_facade : public range_base
{
public:

    auto size() const
        requires requires (Derived& r) {
            rng::subrange(r.begin(), r.end()).size();
        }
    {
        // use iterator in order to evade infinite recursion cause by
        // std::ranges::sized_range
        return rng::subrange(derived().begin(), derived().end()).size();
    }

    auto ssize() const
        requires requires (Derived& r) {
            rng::ssize(rng::subrange(r.begin(), r.end()));
        }
    {
        return rng::ssize(rng::subrange(derived().begin(), derived().end()));
    }

    auto empty() const
        requires requires (Derived& r) {
            rng::subrange(r).empty();
        }
    {
        return rng::subrange(derived()).empty();
    }

    template<typename R = Derived>
    decltype(auto) at(rng::range_difference_t<R> const n)
        requires requires (Derived& r, rng::range_difference_t<R> n) {
                      rng::at(r, n);
                 }
    {
        return rng::at(derived(), n);
    }

    template<typename R = Derived>
    decltype(auto) at(rng::range_difference_t<R> const n) const
        requires requires (Derived const& r, rng::range_difference_t<R> n) {
                      rng::at(r, n);
                 }
    {
        return rng::at(derived(), n);
    }

    template<typename R = Derived>
    decltype(auto) operator[](rng::range_difference_t<R> const n)
        requires requires (Derived& r, rng::range_difference_t<R> n) {
                      rng::at_nocheck(r, n);
                 }
    {
        return rng::at_nocheck(derived(), n);
    }

    template<typename R = Derived>
    decltype(auto) operator[](rng::range_difference_t<R> const n) const
        requires requires (Derived const& r, rng::range_difference_t<R> n) {
                      rng::at_nocheck(r, n);
                 }
    {
        return rng::at_nocheck(derived(), n);
    }

    auto data()
        requires requires (Derived& r) { rng::subrange(r).data(); }
    {
        return rng::subrange(derived()).data();
    }

    auto data() const
        requires requires (Derived const& r) { rng::subrange(r).data(); }
    {
        return rng::subrange(derived()).data();
    }

    decltype(auto) front()
        requires requires (Derived& r) { rng::front(rng::subrange(r)); }
    {
        return rng::front(rng::subrange(derived()));
    }

    decltype(auto) front() const
        requires requires (Derived const& r) { rng::front(rng::subrange(r)); }
    {
        return rng::front(rng::subrange(derived()));
    }

    decltype(auto) back()
        requires requires (Derived& r) { rng::back(rng::subrange(r)); }
    {
        return rng::back(rng::subrange(derived()));
    }

    decltype(auto) back() const
        requires requires (Derived const& r) { rng::back(rng::subrange(r)); }
    {
        return rng::back(rng::subrange(derived()));
    }

    bool operator==(range_facade const&) const = default;
    auto operator<=>(range_facade const&) const = default;

private:
    Derived& derived()
    {
        return reinterpret_cast<Derived&>(*this);
    }

    Derived const& derived() const
    {
        return reinterpret_cast<Derived const&>(*this);
    }
};

template<size_t I, typename T>
    requires std::derived_from<std::remove_cvref_t<T>, range_base>
auto get(T&& r)
{
    if constexpr (I == 0)
        return rng::begin(r);
    else
        return rng::end(r);
}

} // namespace stream9::ranges

namespace std {

template<typename T>
    requires derived_from<remove_cvref_t<T>, stream9::ranges::range_base>
struct tuple_size<T> : integral_constant<size_t, 2>
{};

template<typename T>
    requires derived_from<remove_cvref_t<T>, stream9::ranges::range_base>
struct tuple_element<0, T>
{
    using type = ranges::iterator_t<T>;
};

template<typename T>
    requires derived_from<remove_cvref_t<T>, stream9::ranges::range_base>
struct tuple_element<1, T>
{
    using type = ranges::sentinel_t<T>;
};

} // namespace std

#endif // STREAM9_RANGES_RANGE_FACADE_HPP
