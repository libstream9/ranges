#ifndef STREAM9_RANGES_IS_PREFIX_HPP
#define STREAM9_RANGES_IS_PREFIX_HPP

#include "ranges.hpp"
#include "namespace.hpp"

#include <concepts>
#include <ranges>

namespace stream9::ranges {

namespace _is_prefix {

    template<bool is_proper, typename R1, typename R2>
    bool is_prefix_impl(R1 const& r1, R2 const& r2)
    {
        auto [it1, end1] = rng::unpack(r1);
        auto [it2, end2] = rng::unpack(r2);

        for (; it1 != end1 && it2 != end2; ++it1, ++it2) {
            if (*it1 != *it2) {
                return false;
            }
        }

        if constexpr (is_proper) {
            return it1 == end1 && it2 != end2;
        }
        else {
            return it1 == end1;
        }
    }

    struct api
    {
        template<rng::input_range R1, rng::input_range R2>
            requires std::equality_comparable_with<
                        rng::range_value_t<R1>, rng::range_value_t<R2> >
        bool
        operator()(R1 const& r1, R2 const& r2) const
        {
            return is_prefix_impl<false>(r1, r2);
        }
    };

} // namespace _is_prefix

inline constexpr _is_prefix::api is_prefix;

namespace _is_proper_prefix {

    struct api
    {
        template<rng::input_range R1, rng::input_range R2>
            requires std::equality_comparable_with<
                        rng::range_value_t<R1>, rng::range_value_t<R2> >
        bool
        operator()(R1 const& r1, R2 const& r2) const
        {
            return _is_prefix::is_prefix_impl<true>(r1, r2);
        }
    };

} // namespace _is_proper_prefix

inline constexpr _is_proper_prefix::api is_proper_prefix;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_IS_PREFIX_HPP
